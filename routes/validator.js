const express = require("express");
const request = require('request');

exports.isUrl=async function (req, res) {
    let url=req.query.url;
    let count=0;
    let isSuccess=await validate(url,count);
    if (isSuccess == true){
        res.send("Url is working");
    }else{
        res.send("Url is not working")
    }
}

function validate(url,count) {
    return new Promise((resolve, reject) => {
        let interval;
        function valid() {
            if (url.indexOf("http") != -1 || url.indexOf("https") !=-1)
                url=url
            else
                url="http://"+url

            request(url, function (error, response, body) {
                if (response && response.statusCode == 200){
                    resolve(true)
                }else {
                    count = count+1;
                    if (count == 3 ){
                        clearInterval(interval)
                        interval=0
                        resolve(false)
                    }else {
                        if (count == 1  ){
                            interval=setInterval(valid,10000,url,count);
                        }
                    }

                }
            });
        }
        valid()
    });
}