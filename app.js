'use strict';
const express = require('express');
const path = require('path');

const request = require('request');
const http= require('http');
var cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
const validator = require("./routes/validator");

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

const port = 8001;
app.set('view engine', 'jade');
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({ limit: "50mb",extended: false}));
app.use(cors());

app.use(cors());
app.get('/', function (req, res) {
    res.render('main');
});

app.get('/isUrl',validator.isUrl);

const server = http.createServer(app).listen(port, function () {
    console.log("Express server listening on port " + port);
});



